Set-ExplorerOptions -showHiddenFilesFoldersDrives -showProtectedOSFiles -showFileExtensions
Enable-RemoteDesktop

#Common Packges
cinst dotnet3.5 -y
cinst dotnet4.0 -y
cinst dotnet4.5 -y
cinst dotnet4.5.2 -y
cinst dotnet4.6 -y
cinst dotnet4.6.1 -y
cinst vcredist2010 -y --allow-empty-checksum-secure
cinst autoit.commandline -y --allow-empty-checksum-secure
cinst 7zip  -y   
cinst 7zip.commandline  -y   
cinst 7zip.install  -y
cinst peazip -y
cinst winrar -y
cinst rufus -y
cinst sudo -y
cinst chocolatey-core.extension -y
cinst boxstarter -y
cinst boxstarter.common -y
cinst boxstarter.chocolatey -y
cinst boxstarter.bootstrapper -y
cinst googlechrome  -y   

#Deployment
cinst windows-adk-deploy  -y
cinst packer -y
cinst vagrant -y
cinst python3 -y 
cinst pip -y
cinst openssh -y
cinst pscx -y --allow-empty-checksum-secure
cinst psake -y
cinst carbon -y
cinst chefdk -y --allow-empty-checksum-secure

#Development
cinst nodejs -y --allow-empty-checksum-secure
cinst git  -y   
cinst sourcetree  -y
cinst processhacker -y
cinst atom -y --allow-empty-checksum-secure
cinst golang -y

#Drivers
cinst intel-driver-update-utility -y --allow-empty-checksum-secure
cinst intel-chipset-device-software -y 

#Network
cinst nettime  -y
cinst cyberduck -y 
cinst wireshark -y --allow-empty-checksum-secure
cinst stunnel  -y 

#Utilities
cinst syncthing -y --allow-empty-checksum-secure
cinst ditto -y  --allow-empty-checksum-secure
cinst nirlauncher -y
cinst spacesniffer -y
cinst notepadplusplus -y   
cinst cmder -y --allow-empty-checksum-secure 
cinst putty  -y --allowemptychecksum
cinst nugetpackageexplorer -y --allow-empty-checksum-secure
cinst winscp  -y --allow-empty-checksum-secure
cinst imgburn -y --allow-empty-checksum-secure
cinst ccleaner  -y --allow-empty-checksum-secure
cinst ccenhancer -y --allow-empty-checksum-secure
cinst hwmonitor  -y --allow-empty-checksum-secure
cinst fsviewer  -y 
cinst windirstat -y
cinst greenshot -y

#Media
cinst cccp  -y
cinst k-litecodecpackfull -y
cinst vivaldi  69pre -y   
cinst opera  -y   
cinst vlc  -y --allowemptychecksum
cinst steam  -y   
cinst spotify  -y --allowemptychecksum
cinst foobar2000 -y   
cinst deluge -y   
cinst gpu-z -y
cinst cpu-z -y
cinst myharmony -y
cinst silverlight -y
cinst flashplayerplugin -y
cinst flashplayeractivex -y
cinst flashplayerppapi -y
cinst adobeair -y
cinst adobeshockwaveplayer -y


