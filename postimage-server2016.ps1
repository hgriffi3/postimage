Set-ExplorerOptions -showHiddenFilesFoldersDrives -showProtectedOSFiles -showFileExtensions
Enable-RemoteDesktop

#Common Packges
cinst dotnet3.5 -y
cinst dotnet4.0 -y
cinst dotnet4.5 -y
cinst dotnet4.5.2 -y
cinst dotnet4.6 -y
cinst dotnet4.6.1 -y
cinst autoit.commandline -y
cinst 7zip  -y   
cinst 7zip.commandline  -y   
cinst 7zip.install  -y
cinst peazip -y
cinst winrar -y
cinst rufus -y
cinst teracopy -y
cinst sudo -y
cinst chocolatey-core.extension -y
cinst boxstarter -y
cinst boxstarter.common -y
cinst boxarter.winconfig -y
cinst boxstarter.chocolatey -y
cinst boxstarter.bootstrapper -y
cinst googlechrome  -y   

#Deployment
cinst windows-adk-deploy  -y
cinst packer -y
cinst vagrant -y
cinst pip -y
cinst openssh -y
cinst pscx -y
cinst psake -y
cinst carbon -y
cinst chefdk -y

#Development
cinst nodejs -y
cinst git  -y   
cinst sourcetree  -y
cinst processhacker -y
cinst atom -y
cinst golang -y

#Drivers
cinst intel-driver-update-utility -y
cinst intel-chipset-device-software -y

#Network
cinst nettime  -y
cinst cyberduck -y
cinst wireshark -y
cinst stunnel  -y 

#Utilities
cinst syncthing -y
cinst ditto -y
cinst nirlauncher -y
cinst spacesniffer -y
cinst notepadplusplus -y   
cinst cmder -y   
cinst putty  -y --allowemptychecksum
cinst nugetpackageexplorer  -y   
cinst winscp  -y 
cinst imgburn -y
cinst ccleaner  -y 
cinst ccenhancer -y  
cinst hwmonitor  -y   
cinst fsviewer  -y   
cinst windirstat -y
cinst greenshot -y
cinst clover -y

#Media
cinst gpu-z -y
cinst cpu-z -y


